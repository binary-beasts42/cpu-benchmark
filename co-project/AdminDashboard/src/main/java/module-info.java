module com.example.admindashboard {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.ikonli.javafx;
    requires eu.hansolo.tilesfx;
    requires java.management;
    requires jdk.management;
    requires org.jetbrains.annotations;

    opens com.example.admindashboard to javafx.fxml;
    exports com.example.admindashboard;
    exports com.example.admindashboard.controllers;
    opens com.example.admindashboard.controllers to javafx.fxml;
}