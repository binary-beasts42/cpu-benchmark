package com.example.admindashboard.controllers;

import com.example.admindashboard.SceneSwitcher;
import com.example.admindashboard.cpu_alg.benchmark.Benchmark;
import com.example.admindashboard.cpu_alg.benchmark.BenchmarkType;
import com.example.admindashboard.cpu_alg.benchmark.ReturnInfo;
import com.example.admindashboard.cpu_alg.logging.ConsoleLogger;
import com.example.admindashboard.cpu_alg.logging.ILogging;
import com.example.admindashboard.cpu_alg.logging.TimeUnit;
import com.example.admindashboard.cpu_alg.score.ScoreCalculator;
import com.example.admindashboard.cpu_alg.timing.ITimer;
import com.example.admindashboard.cpu_alg.timing.Timer;
import com.example.admindashboard.models.BenchInfo;
import com.example.admindashboard.models.BenchType;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class EncryptionController implements Initializable {
    class EncryptionTask extends Task<BenchInfo> {
        private int nr_of_encryptions;
        private String algo;
        public EncryptionTask(int nr_of_encryptions) {
            this.nr_of_encryptions = nr_of_encryptions;
        }
        @Override
        protected BenchInfo call() throws Exception {
            ITimer timer = new Timer();
            ILogging logger = new ConsoleLogger();
            ScoreCalculator scoreCalculator = new ScoreCalculator();
            BenchmarkType benchType = BenchmarkType.AES;
            timer.start();
            Benchmark bench = new Benchmark();
            ReturnInfo returnInfo = bench.getScore(benchType, nr_of_encryptions);
            long elapsedTime = timer.stop();
            long score = returnInfo.getScore();
            String result = returnInfo.getResult();
            double executionTime = logger.writeTime("Execution time: ", elapsedTime, TimeUnit.milliseconds);

            BenchInfo benchInfo = new BenchInfo(LocalDate.now(), executionTime, score, result, BenchInfo.AES);
            updateValue(benchInfo);
            for(int i=0; i<(nr_of_encryptions < 100 ? 100 : nr_of_encryptions);i++) {
                updateProgress(i, nr_of_encryptions);
            }
            return benchInfo;
        }
    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SceneSwitcher sw = new SceneSwitcher();
        try {
            String mode = sw.getColorModeFromFile();
            if (mode.equals("light")) {
                sw.setLightMode(parent, imgMode, new ActionEvent());
            } else {
                sw.setDarkMode(parent, imgMode, new ActionEvent());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    private Button btnMode;
    @FXML
    private BorderPane parent;
    @FXML
    private ImageView imgMode;
    @FXML
    private TextArea computerSpecs;
    private Stage stage;
    private Scene scene;
    private Parent root;

    @FXML
    public void changeMode(javafx.event.ActionEvent actionEvent) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        if(SceneSwitcher.isLightMode) {
            sw.setLightMode(parent,imgMode,actionEvent);
            sw.setColorModeInFile("light");
        }else{
            sw.setDarkMode(parent,imgMode,actionEvent);
            sw.setColorModeInFile("dark");
        }
        SceneSwitcher.isLightMode = !SceneSwitcher.isLightMode;
    }

    @FXML
    public void switchToMain(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.mainView, event, "Home Page");
    }

    @FXML
    public void switchToDashboard(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.dashboardView, event, "Dashboard");
    }
    @FXML
    public void switchToPiAlgorithms(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.piAlgorithmsView, event, "PI Algorithms");
    }

    @FXML
    public void switchToCompressionAlgorithm(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.compressionAlgorithmView, event, "Compression Algorithm");
    }

    @FXML
    public void switchToAbout(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.aboutView, event, "About");
    }
    private void openBenchInfoPopup(BenchInfo benchInfo) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.openPopup(SceneSwitcher.benchInfoPopup, benchInfo);
    }

    @FXML
    private TextField no_of_encryptions;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private void run_benchmark(ActionEvent event) {
        if (no_of_encryptions.getText().matches("[0-9]+")) {
            int nr_of_enc = Integer.valueOf(no_of_encryptions.getText());

            EncryptionTask encryptionTask = new EncryptionTask(nr_of_enc);
            encryptionTask.valueProperty().addListener(new ChangeListener<BenchInfo>() {
                @Override
                public void changed(ObservableValue<? extends BenchInfo> observableValue, BenchInfo benchInfo, BenchInfo t1) {
                    try {
                        openBenchInfoPopup(t1);
                    } catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                }
            });
            progressBar.progressProperty().bind((encryptionTask.progressProperty()));

            Thread th = new Thread(encryptionTask);
            th.setDaemon(true);
            th.start();
        } else {
            no_of_encryptions.setText("");
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Incorrect input");
            alert.setHeaderText(null);
            alert.setContentText("The input should only contain digits!");
            alert.show();
        }
    }
}
