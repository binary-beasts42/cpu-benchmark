package com.example.admindashboard;

import com.example.admindashboard.controllers.BenchInfoPopupController;
import com.example.admindashboard.models.BenchInfo;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.*;
import java.time.LocalDate;
import java.util.Objects;

public class SceneSwitcher {
    public static String mainView = "mainView.fxml";
    public static String dashboardView = "dashboardView.fxml";
    public static String piAlgorithmsView = "piAlgorithmsView.fxml";
    public static String compressionAlgorithmView = "encryptionAlgorithmView.fxml";
    public static String aboutView = "aboutView.fxml";
    public static String benchInfoPopup = "benchInfoPopup.fxml";

    private Stage stage;
    private Scene scene;
    private Parent root;
    public static boolean isLightMode = true;

    public void switchScene(String sceneName, ActionEvent event, String title) throws IOException {
        root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource(sceneName)));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setTitle(title);
        scene = new Scene(root,1280,700);
        stage.setScene(scene);
        stage.show();
    }

    public void openPopup(String popupName, BenchInfo benchInfo) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(popupName));
        Parent root1 = loader.load();
        BenchInfoPopupController controller = loader.getController();
        controller.setBenchInfo(benchInfo);

        Stage stage = new Stage();
        stage.getIcons().add(new Image("icon.png"));
        stage.setTitle("Benchmark Result");
        stage.setScene(new Scene(root1));
        stage.show();
    }

    public void setDarkMode(BorderPane borderName, ImageView img, ActionEvent event) {
        borderName.getStylesheets().remove("lightmode.css");
        borderName.getStylesheets().add("darkmode.css");
        Image image = new Image("darkmode.png");
        img.setImage(image);
        isLightMode = false;
    }

    public void setLightMode(BorderPane borderName,ImageView img, ActionEvent event) {
        borderName.getStylesheets().remove("darkmode.css");
        borderName.getStylesheets().add("lightmode.css");
        Image image = new Image("lightmode.png");
        img.setImage(image);
        isLightMode = true;
    }
    public boolean getThemeMode()
    {
        return isLightMode;
    }
    public String getColorModeFromFile() throws IOException {
        File file = new File("settings.txt");
        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;

        if ((st = br.readLine()) != null) {
            return st;
        }
        br.close();
        return "";
    }

    public void setColorModeInFile(String mode) throws IOException {
        File file = new File("settings.txt");
        FileWriter fw = new FileWriter(file);

        fw.write(mode);
        fw.close();
    }
}
