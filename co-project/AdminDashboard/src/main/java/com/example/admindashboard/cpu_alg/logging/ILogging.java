package com.example.admindashboard.cpu_alg.logging;

public interface ILogging {
    void write(long value);
    void write(String value);
    void write(Object ... value);
    void close();

    double writeTime(String s, double time, TimeUnit timeUnit);
}
