package com.example.admindashboard.cpu_alg.benchmark;

public enum BenchmarkType {
    Gauss,
    Leibniz,
    Viete,
    Spigot,
    AES
}
