package com.example.admindashboard.cpu_alg.cpu_bench;

import java.security.NoSuchAlgorithmException;

public interface IBenchmark {
    void run() throws Exception;
    void run(Object ... params) throws Exception;
    void initialise(Object ... params) throws NoSuchAlgorithmException;
    void clean();
    void cancel();
    void warmUp();
    String getResult();
    String getCurrentProgress();
}
