package com.example.admindashboard.controllers;

import com.example.admindashboard.models.BenchInfo;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.DecimalFormat;
import java.util.Objects;


public class BenchInfoPopupController {
    private BenchInfo benchInfo;
    @FXML
    private Label created_at_label;
    @FXML
    private Label execution_time_label;
    @FXML
    private Label score_label;
    @FXML
    private ImageView monsters_view;
    public void setBenchInfo(BenchInfo benchInfo) {
        this.benchInfo = benchInfo;

        created_at_label.setText(String.valueOf(benchInfo.getCreatedAt()));
        execution_time_label.setText(String.valueOf(benchInfo.getExecutionTime()) + "ms");
        score_label.setText(String.valueOf(benchInfo.getScore()));

        long score = benchInfo.getScore();

        Image firstLevel = new Image(getClass().getResourceAsStream("monster4.png"));
        Image secondLevel = new Image(getClass().getResourceAsStream("monster2.png"));
        Image thirdLevel = new Image(getClass().getResourceAsStream("monster3.png"));
        Image fourthLevel = new Image(getClass().getResourceAsStream("monster1.png"));

        if (score >= 0 && score <= 200) {
            monsters_view.setImage(firstLevel);
        } else if (score > 200 && score <= 500) {
            monsters_view.setImage(secondLevel);
        } else if (score > 500 && score <= 999) {
            monsters_view.setImage(thirdLevel);
        } else {
            monsters_view.setImage(fourthLevel);
        }
    }

    @FXML
    private Button save_and_close_btn;

    @FXML
    private void save_and_close(ActionEvent e) throws IOException {
        Stage stage = (Stage) save_and_close_btn.getScene().getWindow();
        File file = new File("bench.txt");
        FileWriter fw = new FileWriter(file, true);
        DecimalFormat df = new DecimalFormat("#.00");
        String info =
                        benchInfo.getID() + "," +
                        benchInfo.getScore() + "," +
                        df.format(benchInfo.getExecutionTime()) + "," +
                        benchInfo.getCreatedAt() + "," +
                        benchInfo.getAlgName() + "\n";

        fw.write(info);
        fw.close();

        stage.close();
    }
}
