package com.example.admindashboard.cpu_alg.timing;

public interface ITimer {
    void start();
    long stop();
    void resume();
    long pause();
}
