package com.example.admindashboard.cpu_alg.cpu_bench;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class CPUDigitsOfPi implements IBenchmark {
    private int n;
    private static final BigDecimal ONE  = new BigDecimal("1");
    private static final BigDecimal TWO  = new BigDecimal("2");
    private static final BigDecimal FOUR = new BigDecimal("4");
    private static final BigDecimal ACC  = new BigDecimal("1E-10000");
    private String result;

    public static BigDecimal sqrt(BigDecimal value, int scale) {
        BigDecimal x0 = new BigDecimal("0");
        BigDecimal x1 = new BigDecimal(Math.sqrt(value.doubleValue()));

        while (!x0.equals(x1)) {
            x0 = x1;
            x1 = value.divide(x0, scale, RoundingMode.HALF_UP);
            x1 = x1.add(x0);
            x1 = x1.divide(TWO, scale, RoundingMode.HALF_UP);
        }

        return x1;
    }
    public void runGauss()
    {
        BigDecimal a = ONE;
        BigDecimal b = ONE.divide(sqrt(TWO, n), n, RoundingMode.HALF_UP);
        BigDecimal t = ONE.divide(FOUR);
        BigDecimal p = ONE;
        BigDecimal oldA;

        while(a.subtract(b).abs().compareTo(ACC) > 0) {
            oldA = a;

            a = oldA.add(b).divide(TWO, n, RoundingMode.HALF_UP);
            b = sqrt(oldA.multiply(b), n);
            t = t.subtract(p.multiply(oldA.subtract(a).multiply(oldA.subtract(a))));
            p = p.multiply(TWO);
        }

        BigDecimal pi = a.add(b).multiply(a.add(b)).divide(FOUR.multiply(t), n, RoundingMode.HALF_UP);
        result = String.valueOf(pi);
    }

    public void runLeibniz() {
        BigDecimal pi = ONE;

        for(int i = 3; i < n; i += 2) {
            if((i - 1) % 4 == 0)
                pi = pi.add(ONE.divide(new BigDecimal(i), n, RoundingMode.HALF_UP));
            else
                pi = pi.subtract(ONE.divide(new BigDecimal(i), n, RoundingMode.HALF_UP));
        }

        pi= pi.multiply(FOUR);
        result = String.valueOf(pi);
    }

    public void runViete() {
        BigDecimal old = sqrt(TWO,n);
        BigDecimal pi = old;

        for (int i =0; i <= n * 2; i++) {
            old = sqrt(TWO.add(old), n);
            pi = pi.multiply(old.divide(TWO));
        }

        TWO.divide(pi, n, RoundingMode.HALF_UP).multiply(TWO);
        result = String.valueOf(TWO.divide(pi, n, RoundingMode.HALF_UP).multiply(TWO));
    }

    @Override
    public void run() {

    }

    @Override
    public void run(Object... params) {
        int option = (int) params[0];
        switch(option)
        {
            case 1:
            {
                runGauss();
                break;
            }
            case 2:
            {
                runLeibniz();
                break;
            }
            case 3:
            {
                runViete();
                break;
            }
            default:
                System.out.println("invalid option, try again!");
        }
    }

    @Override
    public void initialise(Object... params) {
        this.n = (Integer) params[0];
    }

    @Override
    public void clean() {

    }

    @Override
    public void cancel() {

    }

    @Override
    public void warmUp() {
        int copy=n;
        n=50;
        for(int i = 0; i < 1000; i++) {
            runGauss();
        }
        n=copy;
    }

    @Override
    public String getResult() {
        return result;
    }

    @Override
    public String getCurrentProgress() {
        return null;
    }

    public void warmGauss() {
        for(int i = 0; i<10; i++) {
            runGauss();
        }
    }
    public void warmViete() {
        for(int i = 0; i<10; i++) {
            runViete();
        }
    }
    public void warmLeibniz() {
        for(int i = 0; i<10; i++) {
            runLeibniz();
        }
    }
}
