package com.example.admindashboard.cpu_alg.benchmark;

import com.example.admindashboard.cpu_alg.cpu_bench.AES_ENCRYPTION;
import com.example.admindashboard.cpu_alg.cpu_bench.CPUDigitsOfPi;
import com.example.admindashboard.cpu_alg.logging.ILogging;
import com.example.admindashboard.cpu_alg.logging.ConsoleLogger;
import com.example.admindashboard.cpu_alg.logging.TimeUnit;
import com.example.admindashboard.cpu_alg.score.ScoreCalculator;
import com.example.admindashboard.cpu_alg.timing.ITimer;
import com.example.admindashboard.cpu_alg.timing.Timer;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
// How to use:
// Benchmark.getScore(BenchmarkType.Gauss, 10000);

public class Benchmark {
    public ReturnInfo getScore(BenchmarkType benchmark, int workload) {
        ITimer timer = new Timer();
        ILogging log = new ConsoleLogger();
        CPUDigitsOfPi bench = new CPUDigitsOfPi();
        TimeUnit timeUnit = TimeUnit.milliseconds;
        ScoreCalculator score = new ScoreCalculator();

        long finalScore=0;
        ReturnInfo returnInfo = null;
        String result = "";

        bench.initialise(workload);

        switch(benchmark) {
            case Gauss: {
                bench.warmGauss();
                ArrayList<Long> scores = new ArrayList<Long>();
                for(int i=0; i<5; i++) {
                    timer.start();
                    bench.run(1);
                    double time = log.writeTime("Finished in: ", timer.stop(), timeUnit);
                    long scoreGauss = score.gauss(workload, time);
                    scores.add(scoreGauss);
                }
                scores.sort(null);
                finalScore = scores.get(3);
                result = bench.getResult();
                returnInfo = new ReturnInfo(finalScore, result);
                break; }
            case Leibniz: {
                bench.warmLeibniz();
                ArrayList<Long> scores = new ArrayList<Long>();
                for (int i = 0; i < 5; i++) {
                    timer.start();
                    bench.run(2);
                    double time = log.writeTime("Finished in: ", timer.stop(), timeUnit);
                    long scoreGauss = score.leibniz(workload, time);
                    scores.add(scoreGauss);
                }
                scores.sort(null);
                finalScore = scores.get(3);
                result = bench.getResult();
                returnInfo = new ReturnInfo(finalScore, result);
                break;
            }
            case Viete: {
                bench.warmViete();
                ArrayList<Long> scores = new ArrayList<Long>();
                for(int i=0; i<5; i++) {
                    timer.start();
                    bench.run(3);
                    double time = log.writeTime("Finished in: ", timer.stop(), timeUnit);
                    long scoreGauss = score.viete(workload, time);
                    scores.add(scoreGauss);
                }
                scores.sort(null);
                finalScore = scores.get(3);
                result = bench.getResult();
                returnInfo = new ReturnInfo(finalScore, result);
                break;
            }
            case AES: {
                AES_ENCRYPTION aes = new AES_ENCRYPTION();
                try {
                    aes.initialise(workload);
                } catch (NoSuchAlgorithmException e) {
                    throw new RuntimeException(e);
                }
                ArrayList<Long> scores = new ArrayList<Long>();
                for(int i=0; i<5; i++) {
                    timer.start();
                    try {
                        aes.run();
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                    double time = log.writeTime("Finished in: ", timer.stop(), timeUnit);
                    long scoreGauss = score.aes(workload, time);
                    scores.add(scoreGauss);
                }
                scores.sort(null);
                finalScore = scores.get(3);
                result = bench.getResult();
                returnInfo = new ReturnInfo(finalScore, result);
            }
        }
        return returnInfo;
    }
}
