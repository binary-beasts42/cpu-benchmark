package com.example.admindashboard.cpu_alg.logging;

public enum TimeUnit {
    Milliseconds,
    milliseconds,
    ms,
    Seconds,
    seconds,
    sec,
    nanoseconds,
}