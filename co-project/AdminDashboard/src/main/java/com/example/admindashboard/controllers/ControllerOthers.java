package com.example.admindashboard.controllers;

import com.example.admindashboard.SceneSwitcher;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.control.Label;
import javafx.event.ActionEvent;


public class ControllerOthers implements Initializable {

    private Stage stage;
    private Scene scene;
    private Parent root;
    @FXML
    private BorderPane parent;
    @FXML
    private Label cpu_label;
    @FXML
    private ImageView imgMode;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SceneSwitcher sw = new SceneSwitcher();
        try {
            String mode = sw.getColorModeFromFile();
            if (mode.equals("light")) {
                sw.setLightMode(parent, imgMode, new ActionEvent());
            } else {
                sw.setDarkMode(parent, imgMode, new ActionEvent());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @FXML
    public void changeMode(javafx.event.ActionEvent actionEvent) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        if(SceneSwitcher.isLightMode) {
            sw.setLightMode(parent,imgMode,actionEvent);
            sw.setColorModeInFile("light");
        }else{
            sw.setDarkMode(parent,imgMode,actionEvent);
            sw.setColorModeInFile("dark");
        }
        SceneSwitcher.isLightMode = !SceneSwitcher.isLightMode;
    }
    @FXML
    public void switchToMain(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.mainView, event, "Home Page");
    }

    @FXML
    public void switchToDashboard(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.dashboardView, event, "Dashboard");
    }
    @FXML
    public void switchToPiAlgorithms(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.piAlgorithmsView, event, "PI Algorithms");
    }

    @FXML
    public void switchToCompressionAlgorithm(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.compressionAlgorithmView, event, "Compression Algorithm");
    }

    @FXML
    public void switchToAbout(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.aboutView, event, "About");
    }
}