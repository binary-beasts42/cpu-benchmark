package com.example.admindashboard.cpu_alg.cpu_bench;

import java.security.*;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import java.util.Base64;
import java.util.Random;

public class AES_ENCRYPTION implements IBenchmark {
    private int workload;
    private SecretKey key;
    private final int KEY_SIZE = 128;
    private final int DATA_LENGTH = 128;
    private Cipher encryptionCipher;
    private String stringToEncode;

    @Override
    public void run() throws Exception {
        for (int i = 0; i < workload; i++) {
            String encryptedData = encrypt(stringToEncode);
            decrypt(encryptedData);
        }
    }

    @Override
    public void run(Object... params) throws Exception {
        this.workload = (Integer) params[0];
        run();
    }

    private String encrypt(String data) throws Exception {
        byte[] dataInBytes = data.getBytes();
        encryptionCipher = Cipher.getInstance("AES/GCM/NoPadding");
        encryptionCipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] encryptedBytes = encryptionCipher.doFinal(dataInBytes);
        return encode(encryptedBytes);
    }

    private String decrypt(String encryptedData) throws Exception {
        byte[] dataInBytes = decode(encryptedData);
        Cipher decryptionCipher = Cipher.getInstance("AES/GCM/NoPadding");
        GCMParameterSpec spec = new GCMParameterSpec(DATA_LENGTH, encryptionCipher.getIV());
        decryptionCipher.init(Cipher.DECRYPT_MODE, key, spec);
        byte[] decryptedBytes = decryptionCipher.doFinal(dataInBytes);
        return new String(decryptedBytes);
    }

    private String generateRandomString(int nrOfCharacters) {
        int leftLimit = 0; // letter 'a'
        int rightLimit = 255; // letter 'z'
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(nrOfCharacters);
        for (int i = 0; i < nrOfCharacters; i++) {
            int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();
        return generatedString;
    }

    private String encode(byte[] data) {
        return Base64.getEncoder().encodeToString(data);
    }

    private byte[] decode(String data) {
        return Base64.getDecoder().decode(data);
    }

    @Override
    public void initialise(Object... params) throws NoSuchAlgorithmException {
        this.workload = (Integer) params[0];
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(KEY_SIZE);
        key = keyGenerator.generateKey();
        stringToEncode = generateRandomString(10000);
    }

    @Override
    public void clean() {
        encryptionCipher = null;
    }

    @Override
    public void cancel() {
        encryptionCipher = null;
    }

    @Override
    public void warmUp(){
    }

    @Override
    public String getResult() {
        return null;
    }

    @Override
    public String getCurrentProgress() {
        return null;
    }
}