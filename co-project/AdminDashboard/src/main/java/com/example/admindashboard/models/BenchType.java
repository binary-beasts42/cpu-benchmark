package com.example.admindashboard.models;

public enum BenchType {
    PI_BENCH, ENCRYPTION_BENCH
}
