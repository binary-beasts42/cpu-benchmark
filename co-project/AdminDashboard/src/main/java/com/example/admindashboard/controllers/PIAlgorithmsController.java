package com.example.admindashboard.controllers;

import com.example.admindashboard.SceneSwitcher;
import com.example.admindashboard.cpu_alg.benchmark.Benchmark;
import com.example.admindashboard.cpu_alg.benchmark.BenchmarkType;
import com.example.admindashboard.cpu_alg.benchmark.ReturnInfo;
import com.example.admindashboard.cpu_alg.cpu_bench.IBenchmark;
import com.example.admindashboard.cpu_alg.cpu_bench.CPUDigitsOfPi;
import com.example.admindashboard.cpu_alg.logging.ConsoleLogger;
import com.example.admindashboard.cpu_alg.logging.ILogging;
import com.example.admindashboard.cpu_alg.logging.TimeUnit;
import com.example.admindashboard.cpu_alg.score.ScoreCalculator;
import com.example.admindashboard.cpu_alg.timing.ITimer;
import com.example.admindashboard.cpu_alg.timing.Timer;
import com.example.admindashboard.models.BenchInfo;
import com.example.admindashboard.models.BenchType;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Objects;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;

class PITask extends Task<BenchInfo> {
    private int nr_of_digits;
    private String algo;
    public PITask(int nr_of_digits, String algo) {
        this.nr_of_digits = nr_of_digits;
        this.algo = algo;
    }
    @Override
    protected BenchInfo call() throws Exception {
        ITimer timer = new Timer();
        ILogging logger = new ConsoleLogger();
        ScoreCalculator scoreCalculator = new ScoreCalculator();
        BenchmarkType benchType = null;
        String algName = "";
        switch (algo) {
            case "Gauss":
                benchType = BenchmarkType.Gauss;
                algName = BenchInfo.gauss;
                break;
            case "Leibniz":
                benchType = BenchmarkType.Leibniz;
                algName = BenchInfo.leibniz;
                break;
            case "Viete":
                benchType = BenchmarkType.Viete;
                algName = BenchInfo.viete;
                break;
        }
        timer.start();
        Benchmark bench = new Benchmark();
        ReturnInfo returnInfo = bench.getScore(benchType, nr_of_digits);
        long elapsedTime = timer.stop();
        long score = returnInfo.getScore();
        String pi_result = returnInfo.getResult();
        double executionTime = logger.writeTime("Execution time: ", elapsedTime, TimeUnit.milliseconds);

        BenchInfo benchInfo = new BenchInfo(LocalDate.now(), executionTime, score, pi_result, algName);
        updateValue(benchInfo);
        for(int i=0; i<(nr_of_digits < 100 ? 100 : nr_of_digits);i++) {
            updateProgress(i, nr_of_digits);
        }

        return benchInfo;
    }
}


public class PIAlgorithmsController implements Initializable {

    private Stage stage;
    private Scene scene;
    private Parent root;

    @FXML
    private Label specsLabel;
    @FXML
    private BorderPane parent;
    @FXML
    private ImageView imgMode;

    public void changeMode(javafx.event.ActionEvent actionEvent) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        if(SceneSwitcher.isLightMode) {
            sw.setLightMode(parent,imgMode,actionEvent);
            sw.setColorModeInFile("light");
        }else{
            sw.setDarkMode(parent,imgMode,actionEvent);
            sw.setColorModeInFile("dark");
        }
        SceneSwitcher.isLightMode = !SceneSwitcher.isLightMode;
    }
    @FXML
    public void switchToMain(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.mainView, event, "Home Page");
    }

    @FXML
    public void switchToDashboard(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.dashboardView, event, "Dashboard");
    }
    @FXML
    public void switchToPiAlgorithms(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.piAlgorithmsView, event, "PI Algorithms");
    }

    @FXML
    public void switchToCompressionAlgorithm(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.compressionAlgorithmView, event, "Compression Algorithm");
    }

    @FXML
    public void switchToAbout(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.aboutView, event, "About");
    }
    private void openBenchInfoPopup(BenchInfo benchInfo) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.openPopup(SceneSwitcher.benchInfoPopup, benchInfo);
    }

    @FXML
    private TextField digits_textbox;
    private Button generate_btn;
    private int nr_of_digits;
    private String algo;
    @FXML
    private Label digits_label;
    @FXML
    private TextArea result_textbox;

    @FXML
    private ChoiceBox<String> choose_algo;

    @FXML
    public ProgressBar progressBar;
    private final String[] algos = {"Gauss", "Leibniz", "Viete"};

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        choose_algo.getItems().addAll(algos);
        choose_algo.setValue(algos[0]);
        SceneSwitcher sw = new SceneSwitcher();
        try {
            String mode = sw.getColorModeFromFile();
            if (mode.equals("light")) {
                sw.setLightMode(parent, imgMode, new ActionEvent());
            } else {
                sw.setDarkMode(parent, imgMode, new ActionEvent());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @FXML
    private void generatePIDigits(ActionEvent e) {
        result_textbox.setText("");
        if (digits_textbox.getText().matches("[0-9]+")) {
            nr_of_digits = Integer.parseInt(digits_textbox.getText());
            algo = choose_algo.getValue();

            PITask piTask = new PITask(nr_of_digits, algo);
            piTask.valueProperty().addListener(new ChangeListener<BenchInfo>() {
                @Override
                public void changed(ObservableValue<? extends BenchInfo> observableValue, BenchInfo benchInfo, BenchInfo t1) {
                    result_textbox.setText(t1.getResult());
                    try {
                        openBenchInfoPopup(t1);
                    } catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                }
            });
            progressBar.progressProperty().bind((piTask.progressProperty()));
            Thread th = new Thread(piTask);
            th.setDaemon(true);
            th.start();
        } else {
            digits_textbox.setText("");
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Incorrect input");
            alert.setHeaderText(null);
            alert.setContentText("The input should only contain digits!");
            alert.show();
        }
    }
}
