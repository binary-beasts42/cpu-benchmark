package com.example.admindashboard.controllers;

import com.example.admindashboard.SceneSwitcher;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class ControllerMain implements Initializable {
    @FXML
    private LineChart<?, ?> lineChart;
    @FXML
    private Button btnMode;
    @FXML
    private BorderPane parent;
    @FXML
    private ImageView imgMode;
    @FXML
    private TextArea computerSpecs;
    private Stage stage;
    private Scene scene;
    private Parent root;

    public class SpecsTask extends Task<String>{
        @Override
        protected String call() throws Exception {
            String CPUNAME = "";
            String MEMORY = "";
            String OS = "";
            String GPU = "";
            String NAME = "";
            String MANUFACTURER = "";
            String MODEL = "";

            File f = new File("home_specs.txt");

            if (!f.exists()) {
                computerSpecs.setEditable(false);
                try {
                    String filePath = "./specs.txt";
                    /* Use "dxdiag /t" variant to redirect output to a given file */
                    ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/c", "dxdiag", "/t", filePath);
                    computerSpecs.setText("-- Extracting informations about your computer --");
                    Process p = pb.start();
                    p.waitFor();
                    BufferedReader br = new BufferedReader(new FileReader(filePath));
                    String line;
                    while ((line = br.readLine()) != null) {
                        if (line.trim().startsWith("Machine name:")) {
                            NAME = line.trim();
                            NAME = NAME.replace("Machine name:", " ");
                        }
                        if (line.trim().startsWith("System Manufacturer:")) {
                            MANUFACTURER = line.trim();
                            MANUFACTURER = MANUFACTURER.replace("System Manufacturer:", " ");
                        }
                        if (line.trim().startsWith("System Model:")) {
                            MODEL = line.trim();
                            MODEL = MODEL.replace("System Model:", " ");
                        }
                        if (line.trim().startsWith("Operating System:")) {
                            OS = line.trim();
                            OS = OS.replace("Operating System:", " ");
                        }
                        if (line.trim().startsWith("Operating System:")) {
                            OS = line.trim();
                            OS = OS.replace("Operating System:", " ");
                        }
                        if (line.trim().startsWith("Processor:")) {
                            CPUNAME = line.trim();
                            CPUNAME = CPUNAME.replace("Processor:", " ");
                        }
                        if (line.trim().startsWith("Memory:")) {
                            MEMORY = line.trim();
                            MEMORY = MEMORY.replace("Memory:", " ");
                        }
                        if (line.trim().startsWith("Card name:")) {
                            GPU = line.trim();
                            GPU = GPU.replace("Card name:", " ");
                        }
                        if (!CPUNAME.isEmpty() && !MEMORY.isEmpty() && !OS.isEmpty() && !NAME.isEmpty() && !MANUFACTURER.isEmpty() && !MODEL.isEmpty() && !GPU.isEmpty()) {
                            String specs = "Machine name: " + NAME  + "\n" + "Operating System: " + OS + "\n" + "System Manufacturer: " + MANUFACTURER + "\n" +
                                    "System Model: " + MODEL + "\n" + "Processor: " + CPUNAME + "\n" + "RAM Memory: " + MEMORY + "\n" + "Card Name: " + GPU;
                            File ff = new File("home_specs.txt");
                            FileWriter fw = new FileWriter(ff);
                            fw.write(specs);
                            fw.close();
                            computerSpecs.setText(specs);
                            break;
                        }
                    }
                } catch (IOException | InterruptedException ex) {
                    ex.printStackTrace();
                }
            } else {
                BufferedReader buff = new BufferedReader(new FileReader(f));
                String line;
                String specs = "";
                while ((line = buff.readLine()) != null) {
                    specs = specs + line + "\n";
                }
                computerSpecs.setText(specs);
                buff.close();
            }
            return CPUNAME;
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SceneSwitcher sw = new SceneSwitcher();
        try {
            String mode = sw.getColorModeFromFile();
            if (mode.equals("light")) {
                sw.setLightMode(parent, imgMode, new ActionEvent());
            } else {
                sw.setDarkMode(parent, imgMode, new ActionEvent());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        inLineChart();
        SpecsTask specsTask = new SpecsTask();
        Thread th = new Thread(specsTask);
        th.setDaemon(true);
        th.start();
    }
    @FXML
    public void changeMode(javafx.event.ActionEvent actionEvent) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        if(SceneSwitcher.isLightMode) {
            sw.setLightMode(parent,imgMode,actionEvent);
            sw.setColorModeInFile("light");
        }else{
            sw.setDarkMode(parent,imgMode,actionEvent);
            sw.setColorModeInFile("dark");
        }
        SceneSwitcher.isLightMode = !SceneSwitcher.isLightMode;
    }

    @FXML
    public void switchToMain(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.mainView, event, "Home Page");
    }

    @FXML
    public void switchToDashboard(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.dashboardView, event, "Dashboard");
    }
    @FXML
    public void switchToPiAlgorithms(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.piAlgorithmsView, event, "PI Algorithms");
    }

    @FXML
    public void switchToCompressionAlgorithm(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.compressionAlgorithmView, event, "Compression Algorithm");
    }

    @FXML
    public void switchToAbout(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.aboutView, event, "About");
    }

    private void inLineChart(){
        lineChart.setTitle("Algorithms results");
        XYChart.Series Gauss = new XYChart.Series();
        Gauss.setName("Gauss");
        Gauss.getData().add(new XYChart.Data("Apple M1", 953.75));
        Gauss.getData().add(new XYChart.Data("i7-7500U", 706.5));
        Gauss.getData().add(new XYChart.Data("i5-8600K", 901.25));
        Gauss.getData().add(new XYChart.Data("Ryzen 7 5800HS", 749.25));
        Gauss.getData().add(new XYChart.Data("Ryzen 3 2200U", 655.25));
        Gauss.getData().add(new XYChart.Data("i3 5005U", 542.75));
        Gauss.getData().add(new XYChart.Data("i5-4200H", 682.50));
        Gauss.getData().add(new XYChart.Data("i9-9900", 868.75));
        Gauss.getData().add(new XYChart.Data("i5-9300H", 818.50));
        Gauss.getData().add(new XYChart.Data("i7-1065g7", 759.75));

        XYChart.Series Leibniz = new XYChart.Series();
        Leibniz.setName("Leibniz");
        Leibniz.getData().add(new XYChart.Data("Apple M1", 887.25));
        Leibniz.getData().add(new XYChart.Data("i7-7500U", 626.75));
        Leibniz.getData().add(new XYChart.Data("i5-8600K", 767.25));
        Leibniz.getData().add(new XYChart.Data("Ryzen 7 5800HS", 718.00));
        Leibniz.getData().add(new XYChart.Data("Ryzen 3 2200U", 593.75));
        Leibniz.getData().add(new XYChart.Data("i3 5005U", 464.75));
        Leibniz.getData().add(new XYChart.Data("i5-4200H", 443.50));
        Leibniz.getData().add(new XYChart.Data("i9-9900", 798.50));
        Leibniz.getData().add(new XYChart.Data("i5-9300H", 731.75));
        Leibniz.getData().add(new XYChart.Data("i7-1065g7", 630.50));

        XYChart.Series Viette = new XYChart.Series();
        Viette.setName("Viette");
        Viette.getData().add(new XYChart.Data("Apple M1", 65.25));
        Viette.getData().add(new XYChart.Data("i7-7500U", 44.75));
        Viette.getData().add(new XYChart.Data("i5-8600K", 63.75));
        Viette.getData().add(new XYChart.Data("Ryzen 7 5800HS", 49.00));
        Viette.getData().add(new XYChart.Data("Ryzen 3 2200U", 45.50));
        Viette.getData().add(new XYChart.Data("i3 5005U", 45.25));
        Viette.getData().add(new XYChart.Data("i5-4200H", 49.75));
        Viette.getData().add(new XYChart.Data("i9-9900", 57.50));
        Viette.getData().add(new XYChart.Data("i5-9300H", 62.00));
        Viette.getData().add(new XYChart.Data("i7-1065g7", 53.25));

        XYChart.Series Encryption = new XYChart.Series();
        Encryption.setName("Encryption");
        Encryption.getData().add(new XYChart.Data("Apple M1", 1336.00));
        Encryption.getData().add(new XYChart.Data("i7-7500U", 391.00));
        Encryption.getData().add(new XYChart.Data("i5-8600K", 537.50));
        Encryption.getData().add(new XYChart.Data("Ryzen 7 5800HS", 815.50));
        Encryption.getData().add(new XYChart.Data("Ryzen 3 2200U", 407.25));
        Encryption.getData().add(new XYChart.Data("i3 5005U", 234.50));
        Encryption.getData().add(new XYChart.Data("i5-4200H", 371.25));
        Encryption.getData().add(new XYChart.Data("i9-9900", 615.50));
        Encryption.getData().add(new XYChart.Data("i5-9300H", 427.00));
        Encryption.getData().add(new XYChart.Data("i7-1065g7", 679.75));
        lineChart.getData().addAll(Gauss, Leibniz, Viette, Encryption);
        lineChart.lookup(".chart-plot-background").setStyle("-fx-background-color: white;");
        Gauss.getNode().setStyle("-fx-stroke: #94D1BE;");
        Leibniz.getNode().setStyle("-fx-stroke: #B1D5D2;");
        Viette.getNode().setStyle("-fx-stroke: #9DB5B2;");
        Encryption.getNode().setStyle("-fx-stroke: #3B413C;");
    }
}