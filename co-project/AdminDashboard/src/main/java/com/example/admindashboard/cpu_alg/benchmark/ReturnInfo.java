package com.example.admindashboard.cpu_alg.benchmark;

public class ReturnInfo {
    private long score;
    private String result;

    ReturnInfo(long score, String result) {
        this.score = score;
        this.result = result;
    }

    public long getScore() {
        return score;
    }

    public String getResult() {
        return result;
    }
}
