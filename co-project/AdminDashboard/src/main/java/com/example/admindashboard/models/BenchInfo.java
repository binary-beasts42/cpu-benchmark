package com.example.admindashboard.models;

import java.time.LocalDate;
import java.util.UUID;
public class BenchInfo {
    private String ID;
    private LocalDate createdAt;
    private double executionTime;
    private long score;
    private String result;
    private String algName;
    public static String gauss = "Gauss";
    public static String leibniz = "Leibniz";
    public static String viete = "Viete";
    public static String AES = "AES";

    public BenchInfo(LocalDate createdAt, double executionTime, long score, String result, String algName) {
        this.result = result;
        ID = String.valueOf(UUID.randomUUID());
        this.createdAt = createdAt;
        this.executionTime = executionTime;
        this.score = score;
        this.algName = algName;
    }

    public BenchInfo(String ID, LocalDate createdAt, double executionTime, long score, String result, String algName) {
        this.result = result;
        this.ID = ID;
        this.createdAt = createdAt;
        this.executionTime = executionTime;
        this.score = score;
        this.algName = algName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public double getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(double executionTime) {
        this.executionTime = executionTime;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getAlgName() {
        return algName;
    }

    public void setAlgName(String algName) {
        this.algName = algName;
    }
}

