package com.example.admindashboard.controllers;

import com.example.admindashboard.SceneSwitcher;
import com.example.admindashboard.models.BenchInfo;
import com.example.admindashboard.models.BenchType;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import javax.swing.plaf.PanelUI;
import java.awt.*;
import java.io.*;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.ResourceBundle;

public class DashboardController implements Initializable {
    private boolean isLightMode = true;
    @FXML
    private BorderPane parent;
    @FXML
    private ImageView imgMode;
    @FXML
    public void switchToMain(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.mainView, event, "Home Page");
    }

    @FXML
    public void switchToDashboard(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.dashboardView, event, "Dashboard");
    }
    @FXML
    public void switchToPiAlgorithms(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.piAlgorithmsView, event, "PI Algorithms");
    }

    @FXML
    public void switchToCompressionAlgorithm(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.compressionAlgorithmView, event, "Compression Algorithm");
    }

    @FXML
    public void switchToAbout(javafx.event.ActionEvent event) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        sw.switchScene(SceneSwitcher.aboutView, event, "About");
    }

    @FXML
    public void changeMode(javafx.event.ActionEvent actionEvent) throws IOException {
        SceneSwitcher sw = new SceneSwitcher();
        if(SceneSwitcher.isLightMode) {
            sw.setLightMode(parent,imgMode,actionEvent);
            sw.setColorModeInFile("light");
        }else{
            sw.setDarkMode(parent,imgMode,actionEvent);
            sw.setColorModeInFile("dark");
        }
        SceneSwitcher.isLightMode = !SceneSwitcher.isLightMode;
    }

    private ArrayList<BenchInfo> getBenchInfo() throws IOException {
        ArrayList<BenchInfo> benches = new ArrayList<>();

        File file = new File("bench.txt");
        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;

        while ((st = br.readLine()) != null) {
            String[] res = st.split("[,]", 0);
            BenchInfo benchInfo = new BenchInfo(res[0], LocalDate.parse(res[3]), Double.parseDouble(res[2]), Long.parseLong(res[1]), "12", res[4]);
            benches.add(benchInfo);
        }
        return benches;
    }

    @FXML
    private GridPane pi_benches_pane;
    @FXML
    private GridPane enc_benches_pane;

    private void getTable(ArrayList<BenchInfo> benches, GridPane benches_pane) {
        benches_pane.setVgap(20);

        GridPane headerPane = new GridPane();
        Label HeaderIDLabel = new Label("Alg. Name");
        HeaderIDLabel.getStyleClass().add("entity-label-header");
        Label HeaderDateLabel = new Label("Created At");
        HeaderDateLabel.getStyleClass().add("entity-label-header");
        Label HeaderTimeLabel = new Label("Exec. Time(ms)");
        HeaderTimeLabel.getStyleClass().add("entity-label-header");
        Label HeaderScoreLabel = new Label("Score");
        HeaderScoreLabel.getStyleClass().add("entity-label-header");
        headerPane.addColumn(0, HeaderIDLabel);
        headerPane.addColumn(1, HeaderDateLabel);
        headerPane.addColumn(2, HeaderTimeLabel);
        headerPane.addColumn(3, HeaderScoreLabel);
        benches_pane.addRow(0, headerPane);
        headerPane.getStyleClass().add("bench-info-header");
        headerPane.setHgap(150);

        for (int i = 0; i < benches.size(); i++) {
            GridPane pane = new GridPane();
            Label AlgLanel = new Label(benches.get(i).getAlgName());
            AlgLanel.getStyleClass().add("entity-label");
            Label DateLabel = new Label(benches.get(i).getCreatedAt().toString());
            DateLabel.getStyleClass().add("entity-label");
            Label TimeLabel = new Label(String.valueOf(benches.get(i).getExecutionTime()));
            TimeLabel.getStyleClass().add("entity-label");
            Label ScoreLabel = new Label(String.valueOf(benches.get(i).getScore()));
            ScoreLabel.getStyleClass().add("entity-label");
            pane.addColumn(0, AlgLanel);
            pane.addColumn(1, DateLabel);
            pane.addColumn(2, TimeLabel);
            pane.addColumn(3, ScoreLabel);
            benches_pane.addRow(i + 1, pane);
            pane.getStyleClass().add("bench-info-entity");
            pane.setHgap(170);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SceneSwitcher sw = new SceneSwitcher();
        try {
            String mode = sw.getColorModeFromFile();
            if (mode.equals("light")) {
                sw.setLightMode(parent, imgMode, new ActionEvent());
            } else {
                sw.setDarkMode(parent, imgMode, new ActionEvent());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        ArrayList<BenchInfo> benches;
        ArrayList<BenchInfo> pi_benches = new ArrayList<>();
        ArrayList<BenchInfo> enc_benches = new ArrayList<>();
        try {
            benches = getBenchInfo();
            for (BenchInfo b : benches) {
                if (b.getAlgName().equals(BenchInfo.AES)) {
                    enc_benches.add(b);
                } else {
                    pi_benches.add(b);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        getTable(pi_benches, pi_benches_pane);
        getTable(enc_benches, enc_benches_pane);
    }
}
