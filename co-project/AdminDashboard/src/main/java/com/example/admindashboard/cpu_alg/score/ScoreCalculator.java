package com.example.admindashboard.cpu_alg.score;

public class ScoreCalculator {
    // time needs to be in MS
    public long gauss(long workload, double time) {
        long score = Math.round(workload  / Math.sqrt(time));
        return score;
    }
    public long leibniz(long workload, double time) {
        long score = Math.round(workload  / Math.sqrt(time));
        if(workload >= 1000 && workload <5000) {
            return score/2;
        } else if(workload >= 5000 && workload <10000) {
            return score;
        } else if(workload >= 10000 && workload <20000) {
            return Math.round(score * Math.sqrt(3));
        } else if(workload >= 20000 && workload < 30000) {
            return Math.round(score * Math.sqrt(6));
        } else if(workload >= 30000 && workload < 50000) {
            return Math.round(score * Math.sqrt(9));
        } else if(workload >= 50000) {
            return Math.round(score * Math.sqrt(11));
        }
        return score;
    }
    public long viete(long workload, double time) {
        return Math.round(workload  / Math.abs(Math.log(time)));
    }
    public long spigot(long workload, double time) {
        long score = Math.round(workload  / Math.sqrt(time));
        if(workload >= 1000 && workload <5000) {
            return Math.round(score * Math.sqrt(4));
        } else if(workload >= 5000 && workload <10000) {
            return Math.round(score * Math.sqrt(10));
        } else if(workload >= 10000 && workload <20000) {
            return Math.round(score * Math.sqrt(12));
        } else if(workload >= 20000 && workload < 30000) {
            return Math.round(score * Math.sqrt(17));
        } else if(workload >= 30000 ) {
            return Math.round(score * Math.sqrt(23));
        }
        return score;
    }
    public long aes(long workload, double time) {
        long score = Math.round((workload / time) );
        if(workload >= 1000 && workload <5000) {
            return Math.round(score * 79 / Math.sqrt(4));
        } else if(workload >= 5000 && workload <10000) {
            return Math.round(score * 357 / Math.sqrt(35));
        } else if(workload >= 10000 && workload <20000) {
            return Math.round(score * 633 / Math.sqrt(103));
        } else if(workload >= 20000 && workload < 30000) {
            return Math.round(score * 1490 / Math.sqrt(287));
        } else if(workload >= 30000 && workload < 50000) {
            return Math.round(score * 2584 / Math.sqrt(488));
        } else if(workload >= 50000) {
            return Math.round(score * 3833 / Math.sqrt(1323));
        }
        return score;
    }
    public long avg(long ...scores) {
        long total = 0;
        long nr = 0;
        for(long score: scores){
            total += score;
            nr++;
        }
        return total/nr;
    }
}
